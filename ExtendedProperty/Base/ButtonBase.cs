﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using ExtendedProperty.Controls.Config;

namespace ExtendedProperty.Base
{
    public abstract class ButtonBase : Control
    {
        #region States

        public static readonly DependencyProperty IncompleteTimeProperty = DependencyProperty.Register(
            nameof(IncompleteTime), typeof(TimeOfDay), typeof(ButtonBase), new PropertyMetadata(TimeOfDay.None, OnIncompleteTimeChanged));

        public TimeOfDay IncompleteTime
        {
            get { return (TimeOfDay)GetValue(IncompleteTimeProperty); }
            set { SetValue(IncompleteTimeProperty, value); }
        }

        #endregion
        
        public event DependencyPropertyChangedEventHandler IncompleteTimeChanged;

        protected ButtonBase()
        {
            DefaultStyleKey = typeof(ButtonBase);
        }

        private static void OnIncompleteTimeChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            var control = (ButtonBase)obj;

            control.OnIncompleteTimeChanged(args);
            control.IncompleteTimeChanged?.Invoke(obj, args);
        }

        protected virtual void OnIncompleteTimeChanged(DependencyPropertyChangedEventArgs args)
        {
        }
    }
}
