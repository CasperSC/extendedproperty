﻿using System;
using Windows.UI.Xaml;
using ExtendedProperty.Controls.Config;

namespace ExtendedProperty.Base
{
    public class TimeOfDayObject<TValue> : DependencyObject
    {
        public static readonly DependencyProperty CommonValueProperty = DependencyProperty.Register(
            nameof(CommonValue), typeof(TValue), typeof(TimeOfDayObject<TValue>), new PropertyMetadata(null,
                (o, args) => ((TimeOfDayObject<TValue>)o).UpdateActualValue()));

        /// <summary>
        /// Основное значение. Если задано, другие размерные диапазоны игнорируются.
        /// </summary>
        public TValue CommonValue
        {
            get { return (TValue)GetValue(CommonValueProperty); }
            set { SetValue(CommonValueProperty, value); }
        }

        #region Values - Incomplete time of day

        public static readonly DependencyProperty UntilNoonValueProperty = DependencyProperty.Register(
            nameof(UntilNoonValue), typeof(TValue), typeof(TimeOfDayObject<TValue>), new PropertyMetadata(null,
                (o, args) =>
                {
                    var control = (TimeOfDayObject<TValue>)o;
                    control.UpdateActualValue(control.TimeOfDay);
                }));

        /// <summary>
        /// Значение для первой половины дня.
        /// </summary>
        public TValue UntilNoonValue
        {
            get { return (TValue)GetValue(UntilNoonValueProperty); }
            set { SetValue(UntilNoonValueProperty, value); }
        }

        public static readonly DependencyProperty AfternoonValueProperty = DependencyProperty.Register(
            nameof(AfternoonValue), typeof(TValue), typeof(TimeOfDayObject<TValue>), new PropertyMetadata(null,
                (o, args) =>
                {
                    var control = (TimeOfDayObject<TValue>)o;
                    control.UpdateActualValue(control.TimeOfDay);
                }));

        /// <summary>
        /// Значение для второй половины дня.
        /// </summary>
        public TValue AfternoonValue
        {
            get { return (TValue)GetValue(AfternoonValueProperty); }
            set { SetValue(AfternoonValueProperty, value); }
        }

        #endregion

        private static readonly DependencyProperty ActualValueProperty = DependencyProperty.Register(
            nameof(ActualValue), typeof(TValue), typeof(TimeOfDayObject<TValue>), new PropertyMetadata(null));

        public TValue ActualValue
        {
            get { return (TValue)GetValue(ActualValueProperty); }
            private set { SetValue(ActualValueProperty, value); }
        }

        public static readonly DependencyProperty TimeOfDayProperty = DependencyProperty.Register(
            nameof(TimeOfDay), typeof(TimeOfDay), typeof(TimeOfDayObject<TValue>), new PropertyMetadata(TimeOfDay.None, OnTimeOfDayChanged));

        public TimeOfDay TimeOfDay
        {
            get { return (TimeOfDay)GetValue(TimeOfDayProperty); }
            set { SetValue(TimeOfDayProperty, value); }
        }

        private static void OnTimeOfDayChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            var control = (TimeOfDayObject<TValue>)obj;
            control.UpdateActualValue((TimeOfDay)args.NewValue);
        }

        private void UpdateActualValue()
        {
            if (IsTheCommonValueSet())
            {
                ActualValue = CommonValue;
            }
        }

        private void UpdateActualValue(TimeOfDay timeOfDay)
        {
            if (IsTheCommonValueSet())
            {
                return;
            }

            if (IsTheValuesSetForIncompleteTimeOfDay())
            {
                switch (timeOfDay)
                {
                    case TimeOfDay.None:
                        ActualValue = default;
                        break;

                    case TimeOfDay.UntilNoon:
                        ActualValue = UntilNoonValue;
                        break;

                    case TimeOfDay.Afternoon:
                        ActualValue = AfternoonValue;
                        break;

                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        /// <summary>
        /// Установлены значения для времён дня.
        /// </summary>
        private bool IsTheValuesSetForIncompleteTimeOfDay()
        {
            return UntilNoonValue != null && AfternoonValue != null;
        }

        /// <summary>
        /// Установлены значения для времён дня.
        /// </summary>
        private bool IsTheCommonValueSet()
        {
            return CommonValue != null;
        }

        protected TimeOfDayObject()
        {
        }
    }
}
