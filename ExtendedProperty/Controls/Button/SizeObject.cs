﻿using Windows.UI.Xaml;

namespace ExtendedProperty.Controls.Button
{
    public class SizeObject : DependencyObject
    {
        public static readonly DependencyProperty WidthProperty = DependencyProperty.Register(
            nameof(Width), typeof(double), typeof(SizeObject), new PropertyMetadata(10d));

        public double Width
        {
            get { return (double) GetValue(WidthProperty); }
            set { SetValue(WidthProperty, value); }
        }

        public static readonly DependencyProperty HeightProperty = DependencyProperty.Register(
            nameof(Height), typeof(double), typeof(SizeObject), new PropertyMetadata(10d));

        public double Height
        {
            get { return (double) GetValue(HeightProperty); }
            set { SetValue(HeightProperty, value); }
        }
    }
}