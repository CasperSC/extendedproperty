﻿using System.Diagnostics;
using System.Threading;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;
using ExtendedProperty.Base;

namespace ExtendedProperty.Controls.Button
{
    // По другому в XAML не создать объект такого типа
    [DebuggerDisplay("CurrentInstanceNumber = {" + nameof(CurrentInstanceNumber) + "}")]
    public class ExStyle : TimeOfDayObject<StyleObject>
    {
        private static int _counter;

        public int CurrentInstanceNumber { get; }

        public ExStyle()
        {
            CurrentInstanceNumber = Interlocked.Increment(ref _counter);
        }
    }

    [DebuggerDisplay("CurrentInstanceNumber = {" + nameof(CurrentInstanceNumber) + "}")]
    public class ExSize : TimeOfDayObject<SizeObject>
    {
        private static int _counter;

        public int CurrentInstanceNumber { get; }

        public ExSize()
        {
            CurrentInstanceNumber = Interlocked.Increment(ref _counter);
        }
    }

    public class ButtonControl : ButtonBase
    {
        #region Common

        public static readonly DependencyProperty StrokeSizeProperty = DependencyProperty.Register(
            nameof(StrokeSize), typeof(int), typeof(ButtonControl), new PropertyMetadata(null));

        public int StrokeSize
        {
            get { return (int) GetValue(StrokeSizeProperty); }
            set { SetValue(StrokeSizeProperty, value); }
        }

        #endregion

        public static readonly DependencyProperty TStyleProperty = DependencyProperty.Register(
            nameof(TStyle), typeof(ExStyle), typeof(ButtonControl), new PropertyMetadata(null));

        public ExStyle TStyle
        {
            get { return (ExStyle)GetValue(TStyleProperty); }
            set { SetValue(TStyleProperty, value); }
        }

        public static readonly DependencyProperty SizeProperty = DependencyProperty.Register(
            nameof(Size), typeof(ExSize), typeof(ButtonControl), new PropertyMetadata(null));

        public ExSize Size
        {
            get { return (ExSize) GetValue(SizeProperty); }
            set { SetValue(SizeProperty, value); }
        }
        
        public ButtonControl()
        {
            Unloaded += OnUnloaded;
        }

        private void OnUnloaded(object sender, RoutedEventArgs e)
        {
            Unloaded -= OnUnloaded;

            ClearBindingValues();
        }

        protected override void OnApplyTemplate()
        {
            ClearBindingValues();

            BindingOperations.SetBinding(TStyle, ExStyle.TimeOfDayProperty,
                new Binding { Source = this, Path = new PropertyPath(nameof(IncompleteTime)), Mode = BindingMode.OneWay });
            
            BindingOperations.SetBinding(Size, ExSize.TimeOfDayProperty,
                new Binding { Source = this, Path = new PropertyPath(nameof(IncompleteTime)), Mode = BindingMode.OneWay });

            base.OnApplyTemplate();
        }

        private void ClearBindingValues()
        {
            TStyle.ClearValue(ExStyle.TimeOfDayProperty);
            Size.ClearValue(ExSize.TimeOfDayProperty);
        }
    }
}