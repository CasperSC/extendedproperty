﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;

namespace ExtendedProperty.Controls.Button
{
    public class StyleObject : DependencyObject
    {
        public static readonly DependencyProperty PrimaryColorProperty = DependencyProperty.Register(
            nameof(PrimaryColor), typeof(Brush), typeof(StyleObject), new PropertyMetadata(null));

        public Brush PrimaryColor
        {
            get { return (Brush) GetValue(PrimaryColorProperty); }
            set { SetValue(PrimaryColorProperty, value); }
        }

        public static readonly DependencyProperty IconColorProperty = DependencyProperty.Register(
            nameof(IconColor), typeof(Brush), typeof(StyleObject), new PropertyMetadata(null));

        public Brush IconColor
        {
            get { return (Brush) GetValue(IconColorProperty); }
            set { SetValue(IconColorProperty, value); }
        }
    }
}
