﻿namespace ExtendedProperty.Controls.Config
{
    /// <summary>
    /// Время дня.
    /// </summary>
    public enum TimeOfDay
    {
        /// <summary> Не используется. </summary>
        None,

        /// <summary> До полудня. </summary>
        UntilNoon,

        /// <summary> После полудня. </summary>
        Afternoon
    }
}