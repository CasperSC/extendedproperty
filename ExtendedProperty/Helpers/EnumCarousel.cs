﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ExtendedProperty.Helpers
{
    public class EnumCarousel<TEnum>
        where TEnum : struct
    {
        private readonly List<TEnum> _values;
        private int _index;

        public EnumCarousel()
        {
            _values = new List<TEnum>();
            var temp = (TEnum[])Enum.GetValues(typeof(TEnum));

            foreach (TEnum value in temp.Skip(1).ToArray())
            {
                _values.Add(value);
            }
        }

        public TEnum GetNext()
        {
            if (_index == _values.Count)
            {
                _index = 0;
            }

            return _values[_index++];
        }
    }
}