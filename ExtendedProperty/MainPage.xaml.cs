﻿using System;
using System.Threading.Tasks;
using Windows.UI.Core;
using Windows.UI.Xaml.Controls;
using ExtendedProperty.Controls.Config;
using ExtendedProperty.Helpers;

namespace ExtendedProperty
{
    public sealed partial class MainPage : Page
    {
        private readonly EnumCarousel<TimeOfDay> _carousel;

        public MainPage()
        {
            Loaded += OnLoaded;

            _carousel = new EnumCarousel<TimeOfDay>();

            InitializeComponent();
        }

        private void OnLoaded(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            Task.Factory.StartNew(async () =>
            {
                while (true)
                {
                    await Task.Delay(850);
                    await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                    {
                        _button.IncompleteTime = _carousel.GetNext();
                    });
                }
            });
        }
    }
}
